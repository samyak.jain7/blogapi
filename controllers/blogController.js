const Blog = require('../models/blog.js');

// Create a new blog post
exports.createBlog = async (req, res) => {
    try {
      const { title, content, author, categories } = req.body;
      const blog = new Blog({ title, content, author, categories });
      await blog.save();
      res.status(201).json(blog);
    } catch (error) {
      res.status(400).send(`Error: ${error.message}`);
    }
  };
// Get all blog posts
exports.getAllBlogs = async (req, res) => {
  try {
    const blogs = await Blog.find().populate('categories');
    res.status(200).json(blogs);
  } catch (error) {
    res.status(400).send(`Error: ${error.message}`);
  }
};

// Get a blog post by ID
exports.getBlogById = async (req, res) => {
  try {
    const blog = await Blog.findById(req.params.id);
    if (!blog) {
      return res.status(404).send('Blog not found');
    }
    res.status(200).json(blog);
  } catch (error) {
    res.status(400).send(`Error: ${error.message}`);
  }
};

// Update a blog post
exports.updateBlog = async (req, res) => {
    try {
      const { title, content, author, categories } = req.body;
      const blog = await Blog.findByIdAndUpdate(req.params.id, { title, content, author, categories, updatedAt: Date.now() }, { new: true });
      if (!blog) {
        return res.status(404).send('Blog not found');
      }
      res.status(200).json(blog);
    } catch (error) {
      res.status(400).send(`Error: ${error.message}`);
    }
  };

// Delete a blog post
exports.deleteBlog = async (req, res) => {
  try {
    const blog = await Blog.findByIdAndDelete(req.params.id);
    if (!blog) {
      return res.status(404).send('Blog not found');
    }
    res.status(200).send('Blog deleted successfully');
  } catch (error) {
    res.status(400).send(`Error: ${error.message}`);
  }
};
