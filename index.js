require('dotenv').config();
const express = require('express')
const connectDB = require('./config/db')
const app = express()
const PORT = process.env.PORT || 5000;
const userRoutes = require('./routes/userRoutes.js')
const blogRouter = require('./routes/blogRoutes.js')
const categoryRouter = require('./routes/categoryRoutes.js')
const cors = require('cors');

connectDB();

app.use(express.json());

// Enable CORS
app.use(cors());

app.use('/api/users', userRoutes);
app.use('/api/blogs', blogRouter);
app.use('/api/categories', categoryRouter); 

app.listen(PORT, (req,res)=>{
    console.log(`App is running ${PORT}`);
})