const jwt = require('jsonwebtoken');
require('dotenv').config();

const secret = process.env.JWT_SECRET || 'SAMYAKJAIN';

const generateToken = (user) => {
  return jwt.sign({ id: user._id, username: user.username, email: user.email }, secret, {
    expiresIn: '1h'
  });
};

const verifyToken = (token) => {
  return jwt.verify(token, secret);
};

module.exports = {
  generateToken,
  verifyToken
};
