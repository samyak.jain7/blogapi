const jwt = require('jsonwebtoken');
const User = require('../models/users.js'); 

exports.authenticateJWT = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(' ')[1];

    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      req.user = await User.findById(decoded.userId);
      next();
    } catch (error) {
      return res.status(403).json({ message: 'Invalid or expired token please check the token' });
    }
  } else {
    res.status(401).json({ message: 'Authorization header is required' });
  }
};
