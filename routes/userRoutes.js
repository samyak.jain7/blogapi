const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const authenticate = require('../middleware/auth.js');

// Register a new user
router.post('/register', userController.register);

// Login a user
router.post('/login', userController.login);

// Get all users
router.get('/', authenticate, userController.getAllUsers);

// Get user by ID
router.get('/:id', authenticate, userController.getUserById);

// Update user
router.put('/:id', authenticate, userController.updateUser);

// Delete user
router.delete('/:id', authenticate, userController.deleteUser);


module.exports = router;
