const express = require('express');
const router = express.Router();
const blogController = require('../controllers/blogController.js');

// Create a new blog post
router.post('/', blogController.createBlog);

// Get all blog posts
router.get('/', blogController.getAllBlogs);

// Get a blog post by ID
router.get('/:id', blogController.getBlogById);

// Update a blog post
router.put('/:id', blogController.updateBlog);

// Delete a blog post
router.delete('/:id', blogController.deleteBlog);

module.exports = router;
